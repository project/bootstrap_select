(function ($, Drupal, drupalSettings) {
  /**
   * Init selectpicker
   *
   * @type {{attach: Drupal.behaviors.bootstrapSelectInit.attach}}
   */
  Drupal.behaviors.bootstrapSelectInit = {
    attach: function (context, settings) {
      $("select.bootstrap-select", context).selectpicker();
    },
  };
})(jQuery, Drupal, drupalSettings);
