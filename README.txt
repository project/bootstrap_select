CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
3rd party js selectpicker bootstrap-select form widget for Bootstrap theme
sites. (https://developer.snapappointments.com/bootstrap-select/). Library
will be loaded via CDN.

You get a field widget for option types. It can be set via
"Manage form display" page for example:
/admin/structure/types/manage/article/form-display


REQUIREMENTS
------------
All bootstrap dependencies have to be meet.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.
You get a field widget for option types. It can be set via
"Manage form display" page for example:
/admin/structure/types/manage/article/form-display

MAINTAINERS
-----------

Current maintainers:
 * Matthias Vogel (mvogel) - https://www.drupal.org/u/mvogel
