<?php

/**
 * @file
 * Add 3rd party bootstrap-select picker implementation.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function bootstrap_select_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.bootstrap_select':
      $text = file_get_contents(__DIR__ . '/README.txt');
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . Html::escape($text) . '</pre>';
      }
      // Use the Markdown filter to render the README.
      $filter_manager = \Drupal::service('plugin.manager.filter');
      $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
      $config = ['settings' => $settings];
      $filter = $filter_manager->createInstance('markdown', $config);
      return $filter->process($text, 'en');
  }
  return NULL;
}

/**
 * Implements hook_theme().
 */
function bootstrap_select_theme($existing, $type, $theme, $path) {
  return [
    'select__bootstrap_select' => [
      'base hook' => 'select',
      'path' => $path . '/templates',
    ],
  ];
}

/**
 * Implements hook_theme_suggestions_alter().
 */
function bootstrap_select_theme_suggestions_select_alter(array &$suggestions, array $variables, $hook) {
  $element = $variables['element'];
  if (isset($element['#bootstrap_select']) && $element['#bootstrap_select']) {
    $suggestions[] = 'select__bootstrap_select';
  }
}

/**
 * Implements hook_library_info_alter().
 */
function bootstrap_select_library_info_alter(&$libraries, $extension) {
  if ($extension === 'bootstrap_select') {
    // If file exists remove cdn url and use local file.
    if (file_exists('libraries/bootstrap-select/dist/js/bootstrap-select.min.js')) {
      unset($libraries['package']['js']);
      $libraries['package']['js']['/libraries/bootstrap-select/dist/js/bootstrap-select.min.js'] = ['minified' => TRUE];
    }
    if (file_exists('libraries/bootstrap-select/dist/css/bootstrap-select.min.css')) {
      unset($libraries['package']['css']);
      $libraries['package']['css']['theme']['/libraries/bootstrap-select/dist/css/bootstrap-select.min.css'] = ['minified' => TRUE];
    }
  }
}
