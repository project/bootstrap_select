<?php

namespace Drupal\bootstrap_select\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'bootstrap-select' widget.
 *
 * @FieldWidget(
 *   id = "bootstrap_select_widget",
 *   label = @Translation("Bootstrap-Select Widget"),
 *   field_types = {
 *     "entity_reference",
 *     "list_integer",
 *     "list_float",
 *     "list_string"
 *   },
 *   multiple_values = TRUE
 * )
 */
class BootstrapSelectWidget extends OptionsWidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'live_search' => FALSE,
      'placeholder' => '',
      'selected_text_format' => 'value',
      'count_selected_text' => '{0} items selected',
      'actions_box' => FALSE,
      'header' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element += [
      '#type' => 'select',
      '#bootstrap_select' => TRUE,
      '#options' => $this->getOptions($items->getEntity()),
      '#default_value' => $this->getSelectedOptions($items),
      // Do not display a 'multiple' select box if there is only one option.
      '#multiple' => $this->multiple && count($this->options) > 1,
      '#attributes' => $this->getAttributes(),
    ];

    return $element;
  }

  /**
   * Write all settings in the appropriated html attributes if needed.
   *
   * @return array
   *   array for render array #attributes
   */
  protected function getAttributes(): array {
    $attributes = [];
    if ($this->getSetting('live_search')) {
      $attributes['data-live-search'] = 'true';
    }

    if ($this->getSetting('actions_box')) {
      $attributes['data-actions-box'] = 'true';
    }

    if ($this->getSetting('header')) {
      $attributes['data-header'] = Html::escape($this->getSetting('header'));
    }

    if ($this->getSetting('placeholder')) {
      $attributes['title'] = Html::escape($this->getSetting('placeholder'));
    }

    if ($this->getSetting('selected_text_format') === 'count') {
      $attributes['data-selected-text-format'] = $this->getSetting('selected_text_format');
      $attributes['data-count-selected-text'] = Html::escape($this->getSetting('count_selected_text'));
    }

    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $element['live_search'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Live search'),
      '#description' => $this->t('When set to true, adds a search box to the top of the selectpicker dropdown.'),
      '#default_value' => $this->getSetting('live_search'),
    ];

    $element['actions_box'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Actions box'),
      '#description' => $this->t('When set to true, adds two buttons to the top of the dropdown menu (Select All & Deselect All).'),
      '#default_value' => $this->getSetting('actions_box'),
    ];

    $element['placeholder'] = [
      '#title' => $this->t('Placeholder'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('placeholder'),
    ];

    $element['header'] = [
      '#title' => $this->t('Header'),
      '#description' => $this->t('Adds a header to the top of the menu; includes a close button by default'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('header'),
    ];

    $element['selected_text_format'] = [
      '#title' => $this->t('Selected text format'),
      '#type' => 'select',
      '#description' => $this->t('Specifies how the selection is displayed with a multiple select. "values" displays a list of the selected options. "count" displays the total number of selected options.'),
      '#options' => ['value' => 'value', 'count' => 'count'],
      '#default_value' => $this->getSetting('selected_text_format'),
    ];

    $element['count_selected_text'] = [
      '#title' => $this->t('Count selected text'),
      '#description' => $this->t('Sets the format for the text displayed when "Selected text format" is count. {0} is the selected amount. {1} is total available for selection.'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('count_selected_text'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Live Search: @search',
      ['@search' => $this->getSetting('live_search') ? 'On' : 'Off']
    );

    $summary[] = $this->t('Actions Box: @actions',
      ['@actions' => $this->getSetting('actions_box') ? 'On' : 'Off']
    );

    if ($this->getSetting('header')) {
      $summary[] = $this->t('Header: @header',
        ['@header' => $this->getSetting('header')]
      );
    }

    if ($this->getSetting('placeholder')) {
      $summary[] = $this->t('Placeholder: @placeholder',
        ['@placeholder' => $this->getSetting('placeholder')]
      );
    }

    $summary[] = $this->t('Selected Textformat: @selected_text_format',
      ['@selected_text_format' => $this->getSetting('selected_text_format')]
    );

    if ($this->getSetting('selected_text_format') === 'count') {
      $summary[] = $this->t('Count Auswahltext: @count_selected_text',
        ['@count_selected_text' => $this->getSetting('count_selected_text')]
      );
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function sanitizeLabel(&$label) {
    // Select form inputs allow unencoded HTML entities, but no HTML tags.
    $label = Html::decodeEntities(strip_tags($label));
  }

  /**
   * {@inheritdoc}
   */
  protected function supportsGroups() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEmptyLabel() {
    if (!$this->multiple) {
      // Single select: add a 'none' option for non-required fields.
      if (!$this->required) {
        return $this->t('- None -');
      }
      // And a 'select a value' option for required fields that do not come
      // with a value selected.
      if (!$this->has_value) {
        return $this->t('- Select a value -');
      }
    }

    return NULL;
  }

}
